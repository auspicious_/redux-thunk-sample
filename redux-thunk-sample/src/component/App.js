import React from "react";
import Navbar from "../component/Navbar";
import Home from "../component/Home";
import Cart from "../component/Cart";
import { 
    BrowserRouter as Router, 
    Route, 
    Link, 
    Switch 
} from 'react-router-dom';
function App() {
  return (
    <Router> 
    <div className="App">
      <Navbar/>
      <Switch>
                    <Route exact path="/" component={Home}/>
                    <Route path="/cart" component={Cart}/>
                  </Switch>

    </div>
    </Router> 
  );
}

export default App;
